import { Component, OnInit } from '@angular/core';

/**
 * Shows "boosted post" with an upward arrow on top of an activity post
 *
 * Used in both the boost rotator and boosted posts throughout feeds
 */
@Component({
  selector: 'm-activityV2__flag--boosted',
  templateUrl: './boosted-flag.component.html',
  styleUrls: ['./boosted-flag.component.ng.scss'],
})
export class ActivityV2BoostedFlagComponent {}
