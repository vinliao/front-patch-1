import { Component, OnInit } from '@angular/core';

/**
 * Page describing the Privacy Policy at Minds
 */
@Component({
  selector: 'm-aux__privacy',
  templateUrl: './privacy.component.html',
})
export class AuxPrivacyComponent {}
