import { Component, OnInit } from '@angular/core';

/**
 * Page that describes the process for filing a
 * copyright infringement notice
 * related to the Digital Millennium Copyright Act (DMCA)
 */
@Component({
  selector: 'm-aux__dmca',
  templateUrl: './dmca.component.html',
})
export class AuxDmcaComponent {}
