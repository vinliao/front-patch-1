import { Component, OnInit } from '@angular/core';

/**
 * Page describing the terms of service for Minds users
 */
@Component({
  selector: 'm-aux__terms',
  templateUrl: './terms.component.html',
})
export class AuxTermsComponent {}
