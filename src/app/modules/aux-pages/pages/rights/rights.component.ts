import { Component, OnInit } from '@angular/core';

/**
 * Page that describes the Bill of Rights for Minds users
 */
@Component({
  selector: 'm-aux__rights',
  templateUrl: './rights.component.html',
})
export class AuxRightsComponent {}
