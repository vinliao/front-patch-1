import { Component, OnInit } from '@angular/core';

/**
 * Page that provides contact information
 * for those seeking communications with Minds staff
 */
@Component({
  selector: 'm-aux__contact',
  templateUrl: './contact.component.html',
})
export class AuxContactComponent {}
