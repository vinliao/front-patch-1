import { Component } from '@angular/core';

/**
 * Container for auxilliary pages
 * (aka mostly static, informative pages,
 * e.g. Content Policy, Branding)
 */
@Component({
  selector: 'm-aux',
  templateUrl: './aux-pages.component.html',
  styleUrls: ['./aux-pages.component.ng.scss'],
})
export class AuxComponent {}
