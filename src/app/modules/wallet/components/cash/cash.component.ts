import { Component } from '@angular/core';

/**
 * Container for cash section of the wallet
 */
@Component({
  selector: 'm-walletV2__cash',
  templateUrl: './cash.component.html',
})
export class WalletV2CashComponent {}
