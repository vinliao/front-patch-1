import { Component } from '@angular/core';

/**
 * Sidebar widget shown to logged out users.
 * Contains links to Minds resources that might
 * convince someone to learn about and join Minds
 */
@Component({
  selector: 'm-discovery__disclaimer',
  templateUrl: './disclaimer.component.html',
  styleUrls: ['./disclaimer.component.ng.scss'],
})
export class DiscoveryDisclaimerComponent {}
